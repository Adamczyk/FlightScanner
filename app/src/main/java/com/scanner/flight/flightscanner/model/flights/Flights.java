
package com.scanner.flight.flightscanner.model.flights;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flights implements Serializable{

    @SerializedName("termsOfUse")
    @Expose
    private String termsOfUse;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("currPrecision")
    @Expose
    private Integer currPrecision;
    @SerializedName("trips")
    @Expose
    private List<Trip> trips = null;
    @SerializedName("serverTimeUTC")
    @Expose
    private String serverTimeUTC;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Flights() {
    }

    /**
     * 
     * @param termsOfUse
     * @param trips
     * @param serverTimeUTC
     * @param currPrecision
     * @param currency
     */
    public Flights(String termsOfUse, String currency, Integer currPrecision, List<Trip> trips, String serverTimeUTC) {
        super();
        this.termsOfUse = termsOfUse;
        this.currency = currency;
        this.currPrecision = currPrecision;
        this.trips = trips;
        this.serverTimeUTC = serverTimeUTC;
    }

    public String getTermsOfUse() {
        return termsOfUse;
    }

    public void setTermsOfUse(String termsOfUse) {
        this.termsOfUse = termsOfUse;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getCurrPrecision() {
        return currPrecision;
    }

    public void setCurrPrecision(Integer currPrecision) {
        this.currPrecision = currPrecision;
    }

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public String getServerTimeUTC() {
        return serverTimeUTC;
    }

    public void setServerTimeUTC(String serverTimeUTC) {
        this.serverTimeUTC = serverTimeUTC;
    }

}
