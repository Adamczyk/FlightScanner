package com.scanner.flight.flightscanner;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import com.scanner.flight.flightscanner.http.FlightsEndpoint;
import com.scanner.flight.flightscanner.http.StationsEndpoint;
import com.scanner.flight.flightscanner.model.FlightSearchData;
import com.scanner.flight.flightscanner.model.flights.Flights;
import com.scanner.flight.flightscanner.model.stations.Station;
import com.scanner.flight.flightscanner.model.stations.Stations;
import com.scanner.flight.flightscanner.view.CustomNumberPicker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    private AutoCompleteTextView mOriginEditText, mDestinationEditText;
    private CustomNumberPicker mChildrenNumberPicker, mTeenNumberPicker, mAdultNumberPicker;
    private EditText mDateEditText;
    private Button mSearchButton;

    private Calendar mCalendar = Calendar.getInstance();
    private Stations mStations;
    private List<String> mStationNames;

    DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int monthOfYear,
                              int dayOfMonth) {
            mDateEditText.setText(year+ "-" + (monthOfYear+1) + "-" +dayOfMonth);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        init();
    }

    /**
     * Initialization method
     */
    private void init() {
        mOriginEditText = findViewById(R.id.origin_edit_text);
        mDestinationEditText = findViewById(R.id.destination_edit_text);

        mChildrenNumberPicker = findViewById(R.id.children_edit_text);
        mTeenNumberPicker = findViewById(R.id.teens_edit_text);
        mAdultNumberPicker = findViewById(R.id.adults_edit_text);

        mDateEditText = findViewById(R.id.date_edit_text);
        mDateEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if(b) {
                    displayDatePickerDialog();
                }
            }
        });
        mDateEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayDatePickerDialog();
            }
        });

        mSearchButton = findViewById(R.id.button_search);
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    validateInputAndFetchFlights();
                } catch (DataValidationException e) {
                    displayValidationAlert(e.getMessage());
                }
            }
        });

        fetchStations();
    }

    /**
     * Displays new DatePickerDialog with current date
     */
    private void displayDatePickerDialog() {
        new DatePickerDialog(SearchActivity.this, mDateSetListener, mCalendar
                .get(Calendar.YEAR), mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    /**
     * Displays user data validation alert
     * @param message
     */
    private void displayValidationAlert(String message) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(message)
                .setCancelable(true)
                .setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    /**
     * Method validates user input and fetches data if OK
     * @throws DataValidationException if input incorrect
     */
    private void validateInputAndFetchFlights() throws DataValidationException {
        // First validate input data
        FlightSearchData searchData = validateInput();

        // If data correct - open ResultsActivity
        fetchFlights(searchData);
    }

    /**
     * Fetch available stations
     */
    private void fetchStations() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(StationsEndpoint.STATIONS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        StationsEndpoint request = retrofit.create(StationsEndpoint.class);
        request.getStations().enqueue(new Callback<Stations>() {
            @Override
            public void onResponse(Call<Stations> call, Response<Stations> response) {
                mStations = response.body();
                mStationNames = new ArrayList<>();
                for(Station s : response.body().getStations()) {
                    mStationNames.add(s.getName());
                }
                ArrayAdapter<String> adapter =
                        new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_list_item_1, mStationNames);
                mOriginEditText.setAdapter(adapter);
                mDestinationEditText.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Stations> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "A problem occured!", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Fetches a flights list for specified search data
     * @param searchData - search data
     */
    private void fetchFlights(FlightSearchData searchData) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(FlightsEndpoint.FLIGHTS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        FlightsEndpoint request = retrofit.create(FlightsEndpoint.class);
        request.getFlights(
                searchData.getOriginCode(), searchData.getDestinationCode(),
                searchData.getDepartureDate(), searchData.getChildrenCount(),
                searchData.getTeensCount(), searchData.getAdultsCount(),
                searchData.getRoundTrip())
                .enqueue(new Callback<Flights>() {
            @Override
            public void onResponse(Call<Flights> call, Response<Flights> response) {
                Intent intent = new Intent(SearchActivity.this, FlightsActivity.class);
                intent.putExtra(FlightsActivity.FLIGHTS_DATA, response.body());
                startActivity(intent);
            }

            @Override
            public void onFailure(Call<Flights> call, Throwable t) {
                Toast.makeText(SearchActivity.this, "A problem occured!", Toast.LENGTH_LONG).show();
            }
        });
    }

    /**
     * Validates input data and returns FlightSearchData if correct
     * @return FlightSearchData - if validation was ok
     * @throws DataValidationException if validation failed
     */
    private FlightSearchData validateInput() throws DataValidationException {
        if(mStations != null) {
            String origin = mOriginEditText.getText().toString();
            String destination = mDestinationEditText.getText().toString();

            String originCode = null;
            String destinationCode = null;

            int childrenCount = 0;
            int teenCount = 0;
            int adultCount = 0;

            // Validate if passengers number is correct
            try {
                childrenCount = Integer.parseInt(mChildrenNumberPicker.getText().toString());
                teenCount = Integer.parseInt(mTeenNumberPicker.getText().toString());
                adultCount = Integer.parseInt(mAdultNumberPicker.getText().toString());
            } catch (NumberFormatException e) {
                throw new DataValidationException("Passengers number not valid!");
            }

            // Validate if there's at least one passenger
            if(childrenCount + teenCount + adultCount <= 0) {
                throw new DataValidationException("Please specify at least one passenger!");
            }

            // Check if stations are not empty
            if(TextUtils.isEmpty(origin) || TextUtils.isEmpty(destination)) {
                throw new DataValidationException("Departure and arrival stations must not be empty!");
            }

            // Check if stations are correct
            if(!mStationNames.contains(origin) || !mStationNames.contains(destination)) {
                throw new DataValidationException("Departure or arrival invalid! !");
            }

            // Find origin and destination codes
            originCode = mStations.getStation(origin).getCode();
            destinationCode = mStations.getStation(destination).getCode();
            if(TextUtils.isEmpty(originCode) || TextUtils.isEmpty(destinationCode)) {
                throw new DataValidationException("Departure or arrival invalid! !");
            }

            // Check if the date is correct
            Date departureDate;
            SimpleDateFormat format = new SimpleDateFormat("y-M-d");
            try {
                departureDate = format.parse(mDateEditText.getText().toString());
            } catch(ParseException pe) {
                throw new DataValidationException("Incorrect date!");
            }

            // Check if the date is not in the past
            if(departureDate.before(new Date())) {
                throw new DataValidationException("Departure date cannot be in the past!");
            }

            // If everything's ok - return FlightSearchData object with input data
            return new FlightSearchData(originCode, destinationCode, childrenCount,
                    teenCount, adultCount, format.format(departureDate), false);
        } else {
            throw new DataValidationException("There's no data available! Check internet connection!");
        }
    }

    private class DataValidationException extends Exception {
        DataValidationException(String message) {
            super(message);
        }
    }
}
