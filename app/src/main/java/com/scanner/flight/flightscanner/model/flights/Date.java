
package com.scanner.flight.flightscanner.model.flights;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Date implements Serializable {

    @SerializedName("dateOut")
    @Expose
    private String dateOut;
    @SerializedName("flights")
    @Expose
    private List<Flight> flights = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Date() {
    }

    /**
     * 
     * @param flights
     * @param dateOut
     */
    public Date(String dateOut, List<Flight> flights) {
        super();
        this.dateOut = dateOut;
        this.flights = flights;
    }

    public String getDateOut() {
        return dateOut;
    }

    public void setDateOut(String dateOut) {
        this.dateOut = dateOut;
    }

    public List<Flight> getFlights() {
        return flights;
    }

    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }

}
