package com.scanner.flight.flightscanner.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomNumberPicker extends android.support.v7.widget.AppCompatEditText {
    private int mMaxNumber = 99;

    public CustomNumberPicker(Context context) {
        super(context);
        init();
    }

    public CustomNumberPicker(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomNumberPicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * Initialization method
     */
    private void init() {
        addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    int val = Integer.valueOf(s.toString());
                    if(val > mMaxNumber) {
                        s.delete(s.length()-1, s.length());
                    }
                } catch (NumberFormatException e) {

                }
            }
        });
    }

    /**
     * Max number that can be set for this picker
     * @param maxNumber
     */
    public void setMaxNumber(int maxNumber) {
        mMaxNumber = maxNumber;
    }
}
