package com.scanner.flight.flightscanner;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.scanner.flight.flightscanner.model.flights.Flight;
import com.scanner.flight.flightscanner.model.flights.Flights;
import com.scanner.flight.flightscanner.model.flights.Segment;
import com.scanner.flight.flightscanner.model.flights.Trip;

public class SummaryActivity extends AppCompatActivity {
    public static final String SUMMARY_DATA = "summaryData";
    public static final String CURRENCY_DATA = "currencyData";

    private TextView mNumber, mDeparture, mArrival, mDuration;
    private TextView mOrigin, mDestination;
    private TextView mRegularFare, mBusinessFare, mLeisureFare;

    private Flights mFlights;
    private Flight mFlight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        mNumber = findViewById(R.id.flight_number_text_view);
        mOrigin = findViewById(R.id.station_origin_text_view);
        mDestination = findViewById(R.id.station_destination_text_view);
        mDeparture = findViewById(R.id.flight_departure_text_view);
        mArrival = findViewById(R.id.flight_arrival_text_view);
        mDuration = findViewById(R.id.duration_text_view);

        mRegularFare = findViewById(R.id.price_regular_text_view);
        mBusinessFare = findViewById(R.id.price__business_text_view);
        mLeisureFare = findViewById(R.id.price_leisure_text_view);

        mFlights = (Flights) getIntent().getSerializableExtra(FlightsActivity.FLIGHTS_DATA);
        mFlight = (Flight) getIntent().getSerializableExtra(SUMMARY_DATA);

        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    /**
     * Initialize UI with obtained values
     */
    private void init() {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        if(mFlight != null && mFlights != null) {
            Trip trip = mFlights.getTrips().get(0);
            setTitle(trip.getOriginName() + "->" + trip.getDestinationName());
            mOrigin.setText(trip.getOriginName());
            mDestination.setText(trip.getDestinationName());

            mNumber.setText(mFlight.getFlightNumber());
            mDeparture.setText(mFlight.getTime().get(0));
            mArrival.setText(mFlight.getTime().get(1));
            mDuration.setText(mFlight.getDuration());

            mRegularFare.setText(mFlight.getRegularFare().getFares().get(0).getAmount() + " " + mFlights.getCurrency());

            if(mFlight.getBusinessFare() != null) {
                mBusinessFare.setText(mFlight.getBusinessFare().getFares().get(0).getAmount() + " " + mFlights.getCurrency());
            } else {
                mBusinessFare.setText("--");
            }

            if(mFlight.getLeisureFare() != null) {
                mLeisureFare.setText(mFlight.getLeisureFare().getFares().get(0).getAmount() + " " + mFlights.getCurrency());
            } else {
                mLeisureFare.setText("--");
            }
        }
    }
}
