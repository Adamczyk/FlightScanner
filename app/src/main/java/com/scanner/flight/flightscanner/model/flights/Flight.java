
package com.scanner.flight.flightscanner.model.flights;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Flight implements Serializable {

    @SerializedName("faresLeft")
    @Expose
    private Integer faresLeft;
    @SerializedName("flightKey")
    @Expose
    private String flightKey;
    @SerializedName("infantsLeft")
    @Expose
    private Integer infantsLeft;
    @SerializedName("regularFare")
    @Expose
    private RegularFare regularFare;
    @SerializedName("businessFare")
    @Expose
    private BusinessFare businessFare;
    @SerializedName("LeisureFare")
    @Expose
    private LeisureFare leisureFare;
    @SerializedName("segments")
    @Expose
    private List<Segment> segments = null;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("time")
    @Expose
    private List<String> time = null;
    @SerializedName("timeUTC")
    @Expose
    private List<String> timeUTC = null;
    @SerializedName("duration")
    @Expose
    private String duration;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Flight() {
    }

    /**
     * 
     * @param duration
     * @param time
     * @param infantsLeft
     * @param segments
     * @param regularFare
     * @param timeUTC
     * @param faresLeft
     * @param flightNumber
     * @param flightKey
     */
    public Flight(Integer faresLeft, String flightKey, Integer infantsLeft, RegularFare regularFare, List<Segment> segments, String flightNumber, List<String> time, List<String> timeUTC, String duration) {
        super();
        this.faresLeft = faresLeft;
        this.flightKey = flightKey;
        this.infantsLeft = infantsLeft;
        this.regularFare = regularFare;
        this.segments = segments;
        this.flightNumber = flightNumber;
        this.time = time;
        this.timeUTC = timeUTC;
        this.duration = duration;
    }

    public Integer getFaresLeft() {
        return faresLeft;
    }

    public void setFaresLeft(Integer faresLeft) {
        this.faresLeft = faresLeft;
    }

    public String getFlightKey() {
        return flightKey;
    }

    public void setFlightKey(String flightKey) {
        this.flightKey = flightKey;
    }

    public Integer getInfantsLeft() {
        return infantsLeft;
    }

    public void setInfantsLeft(Integer infantsLeft) {
        this.infantsLeft = infantsLeft;
    }

    public RegularFare getRegularFare() {
        return regularFare;
    }

    public void setRegularFare(RegularFare regularFare) {
        this.regularFare = regularFare;
    }

    public BusinessFare getBusinessFare() {
        return businessFare;
    }

    public void setBusinessFare(BusinessFare businessFare) {
        this.businessFare = businessFare;
    }

    public LeisureFare getLeisureFare() {
        return leisureFare;
    }

    public void setLeisureFare(LeisureFare leisureFare) {
        this.leisureFare = leisureFare;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public void setSegments(List<Segment> segments) {
        this.segments = segments;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public List<String> getTimeUTC() {
        return timeUTC;
    }

    public void setTimeUTC(List<String> timeUTC) {
        this.timeUTC = timeUTC;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

}
