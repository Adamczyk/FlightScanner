
package com.scanner.flight.flightscanner.model.flights;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegularFare implements Serializable {

    @SerializedName("fareKey")
    @Expose
    private String fareKey;
    @SerializedName("fareClass")
    @Expose
    private String fareClass;
    @SerializedName("fares")
    @Expose
    private List<Fare> fares = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public RegularFare() {
    }

    /**
     * 
     * @param fares
     * @param fareKey
     * @param fareClass
     */
    public RegularFare(String fareKey, String fareClass, List<Fare> fares) {
        super();
        this.fareKey = fareKey;
        this.fareClass = fareClass;
        this.fares = fares;
    }

    public String getFareKey() {
        return fareKey;
    }

    public void setFareKey(String fareKey) {
        this.fareKey = fareKey;
    }

    public String getFareClass() {
        return fareClass;
    }

    public void setFareClass(String fareClass) {
        this.fareClass = fareClass;
    }

    public List<Fare> getFares() {
        return fares;
    }

    public void setFares(List<Fare> fares) {
        this.fares = fares;
    }

}
