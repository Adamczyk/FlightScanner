package com.scanner.flight.flightscanner.http;

import com.scanner.flight.flightscanner.model.flights.Flights;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FlightsEndpoint {
    String FLIGHTS_BASE_URL="https://sit-nativeapps.ryanair.com";

    @GET("/api/v3/Availability")
    Call<Flights> getFlights(@Query("origin") String stationFrom, @Query("destination") String stationTo,
                             @Query("dateout") String dateOut, @Query("chd") int childrenCount,
                             @Query("teen") int teenagersCount, @Query("adt") int adultsCount,
                             @Query("roundtrip") boolean roundTrip);
}
