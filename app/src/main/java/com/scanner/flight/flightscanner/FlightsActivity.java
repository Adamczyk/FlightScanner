package com.scanner.flight.flightscanner;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.scanner.flight.flightscanner.model.flights.Flight;
import com.scanner.flight.flightscanner.model.flights.Flights;
import com.scanner.flight.flightscanner.model.flights.Trip;
import com.scanner.flight.flightscanner.view.FlightResultsAdapter;

public class FlightsActivity extends AppCompatActivity {
    public static final String FLIGHTS_DATA = "flightsData";

    private RecyclerView mRecyclerView;
    private Flights mFlights;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flights);

        mRecyclerView = findViewById(R.id.results_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mFlights = (Flights) getIntent().getSerializableExtra(FLIGHTS_DATA);

        init();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return super.onOptionsItemSelected(item);
    }

    /**
     * Initialization method
     */
    private void init() {
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        Trip trip = mFlights.getTrips().get(0);
        setTitle(trip.getOriginName() + " -> " + trip.getDestinationName());

        FlightResultsAdapter adapter = new FlightResultsAdapter(mFlights);
        adapter.setOnFlightSelectedListener(new FlightResultsAdapter.OnFlightSelectedListener() {
            @Override
            public void onFlightSelected(Flight flight) {
                openFlightSummary(flight);
            }
        });
        mRecyclerView.setAdapter(adapter);
    }

    /**
     * Open activity with flight summary
     * @param flight
     */
    private void openFlightSummary(Flight flight) {
        Intent intent = new Intent(this, SummaryActivity.class);
        intent.putExtra(SummaryActivity.SUMMARY_DATA, flight);
        intent.putExtra(FlightsActivity.FLIGHTS_DATA, mFlights);
        startActivity(intent);
    }
}
