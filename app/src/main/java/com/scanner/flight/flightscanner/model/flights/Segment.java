
package com.scanner.flight.flightscanner.model.flights;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Segment implements Serializable {

    @SerializedName("segmentNr")
    @Expose
    private Integer segmentNr;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("flightNumber")
    @Expose
    private String flightNumber;
    @SerializedName("time")
    @Expose
    private List<String> time = null;
    @SerializedName("timeUTC")
    @Expose
    private List<String> timeUTC = null;
    @SerializedName("duration")
    @Expose
    private String duration;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Segment() {
    }

    /**
     * 
     * @param duration
     * @param time
     * @param origin
     * @param timeUTC
     * @param flightNumber
     * @param segmentNr
     * @param destination
     */
    public Segment(Integer segmentNr, String origin, String destination, String flightNumber, List<String> time, List<String> timeUTC, String duration) {
        super();
        this.segmentNr = segmentNr;
        this.origin = origin;
        this.destination = destination;
        this.flightNumber = flightNumber;
        this.time = time;
        this.timeUTC = timeUTC;
        this.duration = duration;
    }

    public Integer getSegmentNr() {
        return segmentNr;
    }

    public void setSegmentNr(Integer segmentNr) {
        this.segmentNr = segmentNr;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFlightNumber() {
        return flightNumber;
    }

    public void setFlightNumber(String flightNumber) {
        this.flightNumber = flightNumber;
    }

    public List<String> getTime() {
        return time;
    }

    public void setTime(List<String> time) {
        this.time = time;
    }

    public List<String> getTimeUTC() {
        return timeUTC;
    }

    public void setTimeUTC(List<String> timeUTC) {
        this.timeUTC = timeUTC;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

}
