
package com.scanner.flight.flightscanner.model.flights;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Fare implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("amount")
    @Expose
    private Double amount;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("hasDiscount")
    @Expose
    private Boolean hasDiscount;
    @SerializedName("publishedFare")
    @Expose
    private Double publishedFare;
    @SerializedName("discountInPercent")
    @Expose
    private Integer discountInPercent;
    @SerializedName("hasPromoDiscount")
    @Expose
    private Boolean hasPromoDiscount;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Fare() {
    }

    /**
     * 
     * @param amount
     * @param count
     * @param hasPromoDiscount
     * @param discountInPercent
     * @param type
     * @param publishedFare
     * @param hasDiscount
     */
    public Fare(String type, Double amount, Integer count, Boolean hasDiscount, Double publishedFare, Integer discountInPercent, Boolean hasPromoDiscount) {
        super();
        this.type = type;
        this.amount = amount;
        this.count = count;
        this.hasDiscount = hasDiscount;
        this.publishedFare = publishedFare;
        this.discountInPercent = discountInPercent;
        this.hasPromoDiscount = hasPromoDiscount;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Boolean getHasDiscount() {
        return hasDiscount;
    }

    public void setHasDiscount(Boolean hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    public Double getPublishedFare() {
        return publishedFare;
    }

    public void setPublishedFare(Double publishedFare) {
        this.publishedFare = publishedFare;
    }

    public Integer getDiscountInPercent() {
        return discountInPercent;
    }

    public void setDiscountInPercent(Integer discountInPercent) {
        this.discountInPercent = discountInPercent;
    }

    public Boolean getHasPromoDiscount() {
        return hasPromoDiscount;
    }

    public void setHasPromoDiscount(Boolean hasPromoDiscount) {
        this.hasPromoDiscount = hasPromoDiscount;
    }

}
