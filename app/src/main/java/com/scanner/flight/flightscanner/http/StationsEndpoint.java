package com.scanner.flight.flightscanner.http;

import com.scanner.flight.flightscanner.model.stations.Stations;

import retrofit2.Call;
import retrofit2.http.GET;

public interface StationsEndpoint {
    String STATIONS_BASE_URL="https://tripstest.ryanair.com";

    @GET("/static/stations.json")
    Call<Stations> getStations();
}
