
package com.scanner.flight.flightscanner.model.stations;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Stations {

    @SerializedName("stations")
    @Expose
    private List<Station> stations = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Stations() {
    }

    /**
     * 
     * @param stations
     */
    public Stations(List<Station> stations) {
        super();
        this.stations = stations;
    }

    public List<Station> getStations() {
        return stations;
    }

    public Station getStation(String stationName) {
        for(Station station : stations) {
            if(station.getName().equals(stationName)) {
                return station;
            }
        }
        return null;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

}
