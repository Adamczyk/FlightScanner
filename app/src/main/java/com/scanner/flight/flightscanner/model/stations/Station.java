
package com.scanner.flight.flightscanner.model.stations;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Station {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("alternateName")
    @Expose
    private Object alternateName;
    @SerializedName("alias")
    @Expose
    private List<String> alias = null;
    @SerializedName("countryCode")
    @Expose
    private String countryCode;
    @SerializedName("countryName")
    @Expose
    private String countryName;
    @SerializedName("countryAlias")
    @Expose
    private Object countryAlias;
    @SerializedName("countryGroupCode")
    @Expose
    private String countryGroupCode;
    @SerializedName("countryGroupName")
    @Expose
    private String countryGroupName;
    @SerializedName("timeZoneCode")
    @Expose
    private String timeZoneCode;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("mobileBoardingPass")
    @Expose
    private Boolean mobileBoardingPass;
    @SerializedName("markets")
    @Expose
    private List<Market> markets = null;
    @SerializedName("notices")
    @Expose
    private Object notices;
    @SerializedName("tripCardImageUrl")
    @Expose
    private String tripCardImageUrl;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Station() {
    }

    /**
     * 
     * @param countryName
     * @param markets
     * @param mobileBoardingPass
     * @param alias
     * @param notices
     * @param countryCode
     * @param countryAlias
     * @param code
     * @param alternateName
     * @param timeZoneCode
     * @param countryGroupName
     * @param name
     * @param tripCardImageUrl
     * @param longitude
     * @param latitude
     * @param countryGroupCode
     */
    public Station(String code, String name, Object alternateName, List<String> alias, String countryCode, String countryName, Object countryAlias, String countryGroupCode, String countryGroupName, String timeZoneCode, String latitude, String longitude, Boolean mobileBoardingPass, List<Market> markets, Object notices, String tripCardImageUrl) {
        super();
        this.code = code;
        this.name = name;
        this.alternateName = alternateName;
        this.alias = alias;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.countryAlias = countryAlias;
        this.countryGroupCode = countryGroupCode;
        this.countryGroupName = countryGroupName;
        this.timeZoneCode = timeZoneCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.mobileBoardingPass = mobileBoardingPass;
        this.markets = markets;
        this.notices = notices;
        this.tripCardImageUrl = tripCardImageUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getAlternateName() {
        return alternateName;
    }

    public void setAlternateName(Object alternateName) {
        this.alternateName = alternateName;
    }

    public List<String> getAlias() {
        return alias;
    }

    public void setAlias(List<String> alias) {
        this.alias = alias;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Object getCountryAlias() {
        return countryAlias;
    }

    public void setCountryAlias(Object countryAlias) {
        this.countryAlias = countryAlias;
    }

    public String getCountryGroupCode() {
        return countryGroupCode;
    }

    public void setCountryGroupCode(String countryGroupCode) {
        this.countryGroupCode = countryGroupCode;
    }

    public String getCountryGroupName() {
        return countryGroupName;
    }

    public void setCountryGroupName(String countryGroupName) {
        this.countryGroupName = countryGroupName;
    }

    public String getTimeZoneCode() {
        return timeZoneCode;
    }

    public void setTimeZoneCode(String timeZoneCode) {
        this.timeZoneCode = timeZoneCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Boolean getMobileBoardingPass() {
        return mobileBoardingPass;
    }

    public void setMobileBoardingPass(Boolean mobileBoardingPass) {
        this.mobileBoardingPass = mobileBoardingPass;
    }

    public List<Market> getMarkets() {
        return markets;
    }

    public void setMarkets(List<Market> markets) {
        this.markets = markets;
    }

    public Object getNotices() {
        return notices;
    }

    public void setNotices(Object notices) {
        this.notices = notices;
    }

    public String getTripCardImageUrl() {
        return tripCardImageUrl;
    }

    public void setTripCardImageUrl(String tripCardImageUrl) {
        this.tripCardImageUrl = tripCardImageUrl;
    }

}
