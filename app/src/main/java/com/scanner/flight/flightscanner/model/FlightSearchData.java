package com.scanner.flight.flightscanner.model;

import android.os.Parcel;
import android.os.Parcelable;

public class FlightSearchData{
    private String mOriginCode, mDestinationCode;
    private int mChildrenCount, mTeensCount, mAdultsCount;
    private String mDepartureDate;
    private boolean mRoundTrip;

    public FlightSearchData(String originCode, String destinationCode,
                            int childrenCount, int teensCount, int adultsCount, String departureDate, boolean roundTrip) {
        mOriginCode = originCode;
        mDestinationCode = destinationCode;
        mChildrenCount = childrenCount;
        mTeensCount = teensCount;
        mAdultsCount = adultsCount;
        mDepartureDate = departureDate;
        mRoundTrip = roundTrip;
    }

    public String getOriginCode() {
        return mOriginCode;
    }

    public String getDestinationCode() {
        return mDestinationCode;
    }

    public int getChildrenCount() {
        return mChildrenCount;
    }

    public int getTeensCount() {
        return mTeensCount;
    }

    public int getAdultsCount() {
        return mAdultsCount;
    }

    public String getDepartureDate() {
        return mDepartureDate;
    }

    public boolean getRoundTrip() { return mRoundTrip; }

    @Override
    public String toString() {
        return "(" + mOriginCode + ", " + mDestinationCode + ", " + mDepartureDate +
                ", " + mChildrenCount + ", " + mTeensCount + ", " + mAdultsCount;
    }
}
