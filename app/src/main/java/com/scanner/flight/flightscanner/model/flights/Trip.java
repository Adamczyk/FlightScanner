
package com.scanner.flight.flightscanner.model.flights;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Trip implements Serializable {

    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("originName")
    @Expose
    private String originName;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("destinationName")
    @Expose
    private String destinationName;
    @SerializedName("dates")
    @Expose
    private List<Date> dates = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Trip() {
    }

    /**
     * 
     * @param dates
     * @param origin
     * @param destinationName
     * @param destination
     * @param originName
     */
    public Trip(String origin, String originName, String destination, String destinationName, List<Date> dates) {
        super();
        this.origin = origin;
        this.originName = originName;
        this.destination = destination;
        this.destinationName = destinationName;
        this.dates = dates;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getOriginName() {
        return originName;
    }

    public void setOriginName(String originName) {
        this.originName = originName;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public List<Date> getDates() {
        return dates;
    }

    public void setDates(List<Date> dates) {
        this.dates = dates;
    }

}
