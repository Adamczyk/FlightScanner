
package com.scanner.flight.flightscanner.model.stations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Market {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("group")
    @Expose
    private String group;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Market() {
    }

    /**
     * 
     * @param code
     * @param group
     */
    public Market(String code, String group) {
        super();
        this.code = code;
        this.group = group;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

}
