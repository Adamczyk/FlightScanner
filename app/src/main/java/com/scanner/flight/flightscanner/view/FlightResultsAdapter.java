package com.scanner.flight.flightscanner.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.scanner.flight.flightscanner.R;
import com.scanner.flight.flightscanner.model.flights.Date;
import com.scanner.flight.flightscanner.model.flights.Fare;
import com.scanner.flight.flightscanner.model.flights.Flight;
import com.scanner.flight.flightscanner.model.flights.Flights;

public class FlightResultsAdapter extends RecyclerView.Adapter<FlightResultsAdapter.FlightViewHolder> {
    private Flights mFlights;
    private OnFlightSelectedListener mListener;

    public class FlightViewHolder extends RecyclerView.ViewHolder {
        public Flight mFlight;
        private TextView mNumber, mDeparture, mArrival, mDuration, mPrice;

        public FlightViewHolder(View v) {
            super(v);
            mNumber = v.findViewById(R.id.flight_number_text_view);
            mDeparture = v.findViewById(R.id.flight_departure_text_view);
            mArrival = v.findViewById(R.id.flight_arrival_text_view);
            mDuration = v.findViewById(R.id.duration_text_view);
            mPrice = v.findViewById(R.id.price_text_view);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.onFlightSelected(getFlight());
                }
            });
        }

        public void setFlight(Flight flight) {
            mFlight = flight;
            mNumber.setText(flight.getFlightNumber());
            mDeparture.setText(flight.getTime().get(0));
            mArrival.setText(flight.getTime().get(1));
            mDuration.setText(flight.getDuration());
            Fare fare = flight.getRegularFare().getFares().get(0);
            mPrice.setText(fare.getAmount() + " " + mFlights.getCurrency());
        }

        public Flight getFlight() {
            return mFlight;
        }
    }

    public FlightResultsAdapter(Flights flights) {
        mFlights = flights;
    }

    @Override
    public FlightViewHolder onCreateViewHolder(ViewGroup parent,
                                               int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.flight_result_row, parent, false);

        FlightViewHolder vh = new FlightViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(FlightViewHolder holder, int position) {
        Date date = mFlights.getTrips().get(0).getDates().get(0);
        if(date != null) {
            holder.setFlight(date.getFlights().get(position));
        }
    }

    @Override
    public int getItemCount() {
        Date date = mFlights.getTrips().get(0).getDates().get(0);
        if(date != null) {
            return date.getFlights().size();
        }
        return 0;
    }

    public void setOnFlightSelectedListener(OnFlightSelectedListener listener) {
        mListener = listener;
    }

    public interface OnFlightSelectedListener {
        void onFlightSelected(Flight flight);
    }
}
